#include "thread.h"
#include "socketserver.h"
#include <stdlib.h>
#include <time.h>
#include <list>
#include <vector>
#include <algorithm>

using namespace Sync;

// This thread handles the server operations
class ServerThread : public Thread
{
private:
    SocketServer& server;
public:
    ServerThread(SocketServer& server)
    : server(server)
    {}

    ~ServerThread()
    {
        // Cleanup
	//...
    }

    virtual long ThreadMain()
    {
        // Wait for a client socket connection
        // std::cout << "New connection from 3" << std::endl;
        Socket* newConnection = new Socket(server.Accept());
        // A reference to this pointer 
        Socket& socketReference = *newConnection;

        return 1;
    }

};


int main(void)
{
    std::cout << "I am a server." << std::endl;
	
    // Create our server
    SocketServer server(3000);    

    // Need a thread to perform server operations
    
    while (1) {
        std::cout << "Waiting for a connection..." << std::endl;
        Socket* newConnection = new Socket(server.Accept());
        Socket& socketReference = *newConnection;
        ByteArray data;

        socketReference.Read(data);
        if (data.ToString() == "q") {
            std::cout << "Client disconnected" << std::endl;
            std::string str;
            str = "q";
            ByteArray data2(str);
            socketReference.Write(data2);
            continue;
        }
        if (data.ToString() == "q2") {
            std::cout << "Client disconnected" << std::endl;
            std::string str;
            str = "q";
            ByteArray data2(str);
            socketReference.Write(data2);
            server.Shutdown();
            exit(0);
        }
        std::string str = data.ToString();
        str = str + "??... no, shut up";
        ByteArray data2(str);
        socketReference.Write(data2);
        
    
    }

    FlexWait cinWaiter(1, stdin);
    cinWaiter.Wait();

    // Shut down and clean up the server
    server.Shutdown();

}
