
#include "thread.h"
#include "socket.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace Sync;

int main(void)
{
	// Welcome the user 
	std::cout << "SE3313 Lab 3 Client" << std::endl;

	// Create our socket
	
	while (1){
		std::string message;
		std::cout << "enter a string or q to close client connection or q2 to close server: ";
		std::cin >> message;


		Socket s = Socket("127.0.0.1", 3000);
		ByteArray data = ByteArray(message);
		ByteArray data2;
		//To write to socket and read from socket. You may use ByteArray 
		if (s.Open() < 0)
		{
			std::cout << "couldnt open" << std::endl;
			continue;
		}
			
		s.Write(data);
		s.Read(data2);
		if (data2.ToString() == "q"){
			break;
		}
		std::cout << data2.ToString() << std::endl;
		s.Close();
		
	}
	// s.Close();

	return 0;
}
